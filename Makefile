BINARY_NAME = purged
GO_BUILD = go build
GOARCH ?= amd64
GOOS ?= linux

all: build

clean:
	go clean
	rm -f $(BINARY_NAME)

build:
	GOARCH=$(GOARCH) GOOS=$(GOOS) $(GO_BUILD) -o $(BINARY_NAME) .

run: dep build
	./$(BINARY_NAME)

dep:
	go mod download

test:
	go test .
